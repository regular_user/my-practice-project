package com.example.mobileproject;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.widget.Toast;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.Scan
public class MainActivity extends AppCompatActivity {    private EditText login, password;
    private Button signInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);
        signInButton = (Button) findViewById(R.id.signInButton);
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               
                Toast success = Toast.makeText(getApplicationContext(),
                        "Авторизация прошла успешно!", Toast.LENGTH_SHORT);

                //считывание данных пользователя из файла
                String[] logs = new String[6];
                Scanner read = new Scanner(getResources().openRawResource(R.raw.logs));
                boolean access = false;
                while(read.hasNext()) {
                    logs = read.nextLine().split(" ", 6);
                    if(logs[1] == login.getText().toString() && logs[3] == password.getText().toString()) {
                        access = true;
                        break;
                    }
                    else    access = false;
                }


                if(access){
                    success.show();
                    Intent webActivity = new Intent(MainActivity.this, WebViewActivity.class);
                    webActivity.putExtra("logs", logs);
                    startActivity(webActivity);
                }
		//
		// We don't need it
		//
                else{
 		Toast failure = Toast.makeText(getApplicationContext(),
                        "Введены неверные данные!", Toast.LENGTH_SHORT);
                    failure.show();
                    login.setText("");
                    password.setText("");
                }
            }
        });
    }

}
